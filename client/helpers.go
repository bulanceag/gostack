/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/tenta-browser/polychromatic"
)

func (c *Client) doBareRequest(r *http.Request) error {
	lg := polychromatic.GetLogger("api-bare-request").WithField("url", r.URL)
	lg.Debugf("Making a %s request", r.Method)
	for k, v := range r.Header {
		lg.Tracef(" >: %s: %s", k, v)
	}
	resp, err := c.h.Do(r)
	if err != nil {
		return fmt.Errorf("call error: %w", err)
	}
	if resp.StatusCode != 200 && resp.StatusCode != 204 {
		return fmt.Errorf("call failed '%s': %w", resp.Status, StatusCodeError(resp.StatusCode))
	}
	_, err = ioutil.ReadAll(resp.Body) // We need to force read the request
	if err != nil {
		return fmt.Errorf("read error: %w", err)
	}
	return nil
}

func (c *Client) doWithArgs(r *http.Request, args map[string]string, target interface{}) error {
	lg := polychromatic.GetLogger("api-arg-request").WithField("url", r.URL)
	lg.Debugf("Making a %s request", r.Method)
	vals := &url.Values{}
	for k, v := range args {
		vals.Add(k, v)
	}
	r.URL.RawQuery = vals.Encode()
	lg.Trace(r.URL.RawQuery)
	for k, v := range r.Header {
		lg.Tracef(" >: %s: %s", k, v)
	}
	resp, err := c.h.Do(r)
	if err != nil {
		return fmt.Errorf("call error: %w", err)
	}
	lg.Debugf("Got response %s [%d]", resp.Status, resp.StatusCode)
	for k, v := range resp.Header {
		lg.Tracef(" <: %s: %s", k, v)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("read error: %w", err)
	}
	if len(data) < 1025 {
		var pretty bytes.Buffer
		pretty.WriteByte('\n')
		err = json.Indent(&pretty, data, "", "  ")
		pretty.WriteByte('\n')
		if err == nil {
			lg.Trace(pretty.String())
		}
	} else {
		lg.Tracef("<snipped %d bytes of JSON>", len(data))
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("call failed '%s': %w", resp.Status, StatusCodeError(resp.StatusCode))
	}
	err = json.Unmarshal(data, &target)
	if err != nil {
		return fmt.Errorf("unmarshall error: %w", err)
	}
	return nil
}

func (c *Client) doWithBody(r *http.Request, body map[string]interface{}, target interface{}) error {
	lg := polychromatic.GetLogger("api-body-request").WithField("url", r.URL)
	lg.Debugf("Making a %s request", r.Method)
	err := addBody(r, body, lg)
	if err != nil {
		return fmt.Errorf("body failure: %w", err)
	}
	for k, v := range r.Header {
		lg.Tracef(" >: %s: %s", k, v)
	}
	resp, err := c.h.Do(r)
	if err != nil {
		return fmt.Errorf("call error: %w", err)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("read error: %w", err)
	}
	lg.Debugf("Got response %s [%d]", resp.Status, resp.StatusCode)
	for k, v := range resp.Header {
		lg.Tracef(" <: %s: %s", k, v)
	}
	if len(data) < 1025 {
		var pretty bytes.Buffer
		pretty.WriteByte('\n')
		err = json.Indent(&pretty, data, "", "  ")
		pretty.WriteByte('\n')
		if err == nil {
			lg.Trace(pretty.String())
		}
	} else {
		lg.Tracef("<snipped %d bytes of JSON>", len(data))
	}
	if resp.StatusCode != 200 {
		lg.Tracef("\n%s\n", data)
		return fmt.Errorf("call failed '%s': %w", resp.Status, StatusCodeError(resp.StatusCode))
	}
	err = json.Unmarshal(data, &target)
	if err != nil {
		return fmt.Errorf("unmarshall error: %w", err)
	}
	return nil
}

func (c *Client) deleteRequest(path string) (*http.Request, error) {
	return c.createRequest("DELETE", path)
}

func (c *Client) patchRequest(path string) (*http.Request, error) {
	return c.createRequest("PATCH", path)
}

func (c *Client) postRequest(path string) (*http.Request, error) {
	return c.createRequest("POST", path)
}

func (c *Client) putRequest(path string) (*http.Request, error) {
	return c.createRequest("PUT", path)
}

func (c *Client) getRequest(path string) (*http.Request, error) {
	return c.createRequest("GET", path)
}

func (c *Client) createRequest(method string, path string) (*http.Request, error) {
	req, err := http.NewRequest(method, c.endp+path, nil)
	if err != nil {
		return nil, fmt.Errorf("could not create %s request to %s: %w", method, path, err)
	}
	req.Header.Add("User-Agent", "GoStack Client/1.0")
	req.Header.Add("X-More-Info", "jwriteclub@gmail.com")
	req.Header.Add("Accept", "application/json")
	return req, nil
}

func addBody(r *http.Request, fields map[string]interface{}, lg *logrus.Entry) error {
	data, err := json.Marshal(fields)
	if err != nil {
		lg.Debugf("Unable to marshall body %#v: %w", data, err)
		return fmt.Errorf("could not add request body: %w", err)
	}
	if len(data) < 1025 {
		var pretty bytes.Buffer
		pretty.WriteByte('\n')
		err = json.Indent(&pretty, data, "", "  ")
		pretty.WriteByte('\n')
		if err == nil {
			lg.Trace(pretty.String())
		}
	} else {
		lg.Tracef("<snipped %d bytes of JSON>", len(data))
	}
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("Content-Length", fmt.Sprintf("%d", len(data)))
	r.Body = ioutil.NopCloser(bytes.NewBuffer(data))
	return nil
}

func (c *Client) addAuth(r *http.Request) error {
	if time.Until(c.t.ExpiresAt) < time.Minute {
		polychromatic.GetLogger("re-auth").Infof("Expires at %s, [until %d, minute %d]", c.t.ExpiresAt.String(), time.Until(c.t.ExpiresAt), time.Minute)
		err := c.Authenticate()
		if err != nil {
			return fmt.Errorf("unable to re-authenticate: %w", err)
		}
	}
	r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.t.Token))
	return nil
}

/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package dns

import "strings"

type Zone struct {
	StackID     string            `json:"stackId"`
	AccountID   string            `json:"accountId"`
	ID          string            `json:"id"`
	Domain      string            `json:"domain"`
	Version     string            `json:"version"`
	Labels      map[string]string `json:"labels"`
	Created     string            `json:"created"`
	Updated     string            `json:"updated"`
	NameServers []string          `json:"nameservers"`
	Verified    string            `json:"verified"`
	Status      string            `json:"status"`
	Disabled    bool              `json:"disabled"`
}

func (z Zone) String() string {
	sb := strings.Builder{}
	sb.WriteString(z.Domain)
	sb.WriteByte(' ')
	sb.WriteByte('[')
	sb.WriteString(strings.ToLower(z.Status))
	sb.WriteByte('/')
	if z.Disabled {
		sb.WriteString("disabled")
	} else {
		sb.WriteString("enabled")
	}
	sb.WriteByte(']')
	return sb.String()
}

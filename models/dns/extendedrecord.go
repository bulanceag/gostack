/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package dns

import (
	"fmt"
	"github.com/wpalmer/gozone"
	"strings"
)

type ExtendedRecord struct {
	DomainName string
	BaseName   string // The FQDN of the zone, copied in here for ease of use TODO: Do we need this?
	TimeToLive int64  // uint32, expanded and signed to allow for "unset" indicator
	Class      gozone.RecordClass
	Type       gozone.RecordType
	Data       []string
	Comment    string
	ZoneId     string // The ID of the zone
	RecordId   string // The ID of the individual record, for updates
}

func (r ExtendedRecord) String() string {
	spec := []string{r.DomainName}

	if r.TimeToLive != -1 {
		spec = append(spec, fmt.Sprintf("%d", r.TimeToLive))
	}

	if r.Class != gozone.RecordClass_UNKNOWN {
		spec = append(spec, r.Class.String())
	}

	if r.Type != gozone.RecordType_UNKNOWN {
		spec = append(spec, r.Type.String())
	}

	if len(r.Data) != 0 {
		spec = append(spec, strings.Join(r.Data, " "))
	}

	if len(r.Comment) != 0 {
		spec = append(spec, "; ")
		spec = append(spec, r.Comment)
	}

	if len(r.RecordId) != 0 {
		spec = append(spec, "; record_id: ")
		spec = append(spec, r.RecordId)
	}

	return strings.Join(spec, " ")
}

// Returns -1 if a is less than b, 0 if equal and 1 if greater
func Compare(a, b ExtendedRecord) int {
	// First double check to see if one record is a SOA
	if a.Type == gozone.RecordType_SOA && b.Type != gozone.RecordType_SOA {
		return -1
	}
	if b.Type == gozone.RecordType_SOA && a.Type != gozone.RecordType_SOA {
		return 1
	}
	// Then sort by name
	if a.DomainName != b.DomainName {
		if len(a.DomainName) != len(b.DomainName) {
			// First by length
			if len(a.DomainName) < len(b.DomainName) {
				return -1
			} else {
				return 1
			}
		} else {
			// Then lexicographically
			return strings.Compare(a.DomainName, b.DomainName)
		}
	}
	// Then, records are organized by class
	if a.Class != b.Class {
		if a.Class < b.Class {
			return -1
		} else {
			return 1
		}
	}
	// Then records are organized by type
	if a.Type != b.Type {
		// IMPORTANT: We don't need to check for SOA again here, since we already checked it above
		// Otherwise, just compare the types
		if a.Type < b.Type {
			return -1
		} else {
			return 1
		}
	}
	// Then by TTL
	if a.TimeToLive != b.TimeToLive {
		if a.TimeToLive < b.TimeToLive {
			return -1
		} else {
			return 1
		}
	}
	// Finally sort by the data fields
	if len(a.Data) != len(b.Data) {
		if len(a.Data) < len(b.Data) {
			return -1
		} else {
			return 1
		}
	}
	for k := range a.Data {
		if cmp := strings.Compare(a.Data[k], b.Data[k]); cmp != 0 {
			return cmp
		}
	}
	// At this point the records are demonstrably identical, so we'll just say that a is not less than b
	return 0
}

func AlmostEqual(a, b ExtendedRecord) bool {
	if a.Type != b.Type {
		return false
	}
	if a.Class != b.Class {
		return false
	}
	if a.DomainName != b.DomainName {
		return false
	}
	if len(a.Data) != len(b.Data) {
		return false
	}
	return true
}

/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package dns

import (
	"sort"
)

// Sort records defines a total ordering over any set of DNS
// records, ensuring that even if zones are changed manually,
// or an API returns records in a different order that it
// is possible to ensure a complete and ordered set of records
// for a zone when comparing them later.
func SortRecords(records []ExtendedRecord) []ExtendedRecord {
	sort.Slice(records, func(i, j int) bool {
		a := records[i]
		b := records[j]
		return Compare(a, b) < 0
	})
	return records
}

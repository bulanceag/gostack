/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package dns

import (
	"encoding/json"
	"fmt"
	"github.com/tenta-browser/polychromatic"
	"github.com/wpalmer/gozone"
	"strings"
)

type ResourceRecord struct {
	Id      string            `json:"id"`
	ZoneId  string            `json:"zoneId"`
	Name    string            `json:"name"`
	Type    string            `json:"type"`
	Class   string            `json:"class"`
	TTL     int               `json:"ttl"`
	Data    string            `json:"data"`
	Labels  map[string]string `json:"labels"`
	Created string            `json:"created"`
	Updated string            `json:"updated"`
}

func (r ResourceRecord) ExtendedRecord(base string) ExtendedRecord {
	name := base
	if r.Name != "@" && r.Name != "" {
		name = fmt.Sprintf("%s.%s", r.Name, base)
	}
	data := make([]string, 0)
	switch r.GRType() {
	case gozone.RecordType_A:
		fallthrough
	case gozone.RecordType_AAAA:
		fallthrough
	case gozone.RecordType_CNAME:
		fallthrough
	case gozone.RecordType_TXT:
		fallthrough
	case gozone.RecordType_SRV:
		data = append(data, r.Data)
	default: // RecordType_NS, RecordType_MX

	}
	cmt := NoteComment{}
	if d, ok := r.Labels[KeyNoteComment]; ok {
		err := json.Unmarshal([]byte(d), &cmt)
		if err != nil {
			polychromatic.GetLogger("extended-record-converter").Errorf("Unable to decode comment: %w", err)
		}
	}
	payload := []string{r.Data}
	if r.GRType() == gozone.RecordType_MX {
		payload = strings.Split(r.Data, " ")
	}
	ret := ExtendedRecord{
		DomainName: CanonicalizeName(name),
		TimeToLive: int64(r.TTL),
		Class:      r.GRClass(),
		Type:       r.GRType(),
		Data:       payload,
		Comment:    cmt.Comment,
		ZoneId:     r.ZoneId,
		RecordId:   r.Id,
	}
	return ret
}

func (r ResourceRecord) GRClass() gozone.RecordClass {
	switch r.Class {
	case "UNKNOWN":
		return gozone.RecordClass_UNKNOWN
	case "IN":
		return gozone.RecordClass_IN
	case "CS":
		return gozone.RecordClass_CS
	case "CH":
		return gozone.RecordClass_CH
	case "HS":
		return gozone.RecordClass_HS
	default:
		// TODO: should we push an error up here
		panic(fmt.Sprintf("invalid dns class %s", r.Class))
	}
}

func (r ResourceRecord) GRType() gozone.RecordType {
	switch r.Type {
	case "A":
		return gozone.RecordType_A
	case "AAAA":
		return gozone.RecordType_AAAA
	case "CNAME":
		return gozone.RecordType_CNAME
	case "TXT":
		return gozone.RecordType_TXT
	case "MX":
		return gozone.RecordType_MX
	case "SRV":
		return gozone.RecordType_SRV
	case "NS":
		return gozone.RecordType_NS
	default:
		// TODO: should we push an error up here
		panic(fmt.Sprintf("invalid dns type %s", r.Type))
	}
}

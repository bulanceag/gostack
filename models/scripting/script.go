/*
 * GoStack API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package scripting

import "encoding/base64"

type Script struct {
	Id      string   `json:"id"`
	StackId string   `json:"stackId"`
	SiteId  string   `json:"siteId"`
	Name    string   `json:"name"`
	Version string   `json:"version"`
	Code    string   `json:"code"`
	Paths   []string `json:"paths"`
	Created string   `json:"createdAt"`
	Updated string   `json:"updatedAt"`
}

func (s *Script) PutCode(code []byte) {
	s.Code = base64.StdEncoding.EncodeToString(code)
}

func (s *Script) GetCode() ([]byte, error) {
	return base64.StdEncoding.DecodeString(s.Code)
}

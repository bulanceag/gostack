GoStack
=======

An opinionated Go implementation of the [StackPath API](https://stackpath.dev).

Installation
------------

`go get bitbucket.org/jwriteclub/gostack`

Usage
-----

Make sure that you have a viper config which contains the following keys:

* `stack-client-id`: The StackPath API Client ID
* `stack-client-key`: The StackPath API Client Key
* `stack-id`: The ID of the main stack to work on (for APIs that are stack specific)

Status
------

_Note: This is still a work in progress_

Implemented parts of the API:

- [x] Authentication
- [x] DNS Records and record management
- [x] Serverless Scripting
- [x] Content Delivery Network (partial)
- [ ] Workloads
- [x] Cache control
- [ ] SSL
- [ ] Monitoring
- [ ] Object Storage
- [x] Web Application Firewall (partial)
- [x] Sites (partial)
- [ ] Stacks
- [ ] Accounts and Users

License
-------

Copyright 2021 Christopher O'Connell

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
